//
//  AppInitializer.swift
//  WeatherApp
//
//  Created by Anton Zuev on 5/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import Foundation
import UIKit

class AppInitializer {
    
    let rootWireFrame: RootWireFrame = RootWireFrame()
    
    func installRootViewControllerIntoWindow(window: UIWindow) {
        
        rootWireFrame.window = window
        
        // INIT MAIN MODULE
        
        let mainWireFrame = MainWireFrame()
        let mainInteractor = MainViewInteractor(dataManager: WeatherDataManagerWeb())
        let mainPresenter = MainViewPresenter(view: mainWireFrame.mainViewController, interactor: mainInteractor, mainWireFrame: mainWireFrame)
        mainWireFrame.mainViewController.presenter=mainPresenter
        mainWireFrame.rootWireFrame = rootWireFrame
        mainWireFrame.showMainModuleAsRoot()
        
    }
}

//
//  LocationManager.swift
//  WeatherApp
//
//  Created by Anton Zuev on 3/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import CoreLocation

protocol LocationObserverProtocol : class {
    func locationUpdate(locations: [CLLocation])
    func autorizationStatusDidChange(status: CLAuthorizationStatus)
}

class LocationUtility : NSObject{
    
    static let shared = LocationUtility()
    private let locationManager = CLLocationManager()
    private var observers: [LocationObserverProtocol] = []
    
    ///Add observer for location updates
    /// - Parameter newObserver: observer that needs to be added
    func addObserver(newObserver:LocationObserverProtocol){
        //if it's first observer,start location updates
        if observers.count == 0{
            locationManager.startUpdatingLocation()
        }
        observers.append(newObserver)
    }
    ///Remove observer and not send location updates for him
    /// - Parameter observerToRemove: observer that needs to be removed
    func removeObserver(observerToRemove:LocationObserverProtocol){
        if let index = observers.firstIndex(where: {$0 === observerToRemove}) {
            observers.remove(at: index)
        }
        //if there is no observer, stop location updates
        if observers.count == 0 {
            locationManager.stopUpdatingLocation()
        }
    }
    ///Supress creation of the class with initializer from outside of the class
    private override init(){
        super.init()
        locationManager.requestAlwaysAuthorization()
        locationManager.delegate = self
    }
    
    ///Stop location updates and remove all observes
    deinit {
        locationManager.stopUpdatingLocation();
        observers.removeAll()
    }
}

extension LocationUtility: CLLocationManagerDelegate {
    ///Comunicate to all observers location updates
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        for observer in observers {
            observer.locationUpdate(locations: locations)
        }
    }
    ///Tells to all observers that autorization status has changes
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        for observer in observers {
            observer.autorizationStatusDidChange(status: status)
        }
    }
}

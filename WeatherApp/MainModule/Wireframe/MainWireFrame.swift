//
//  MainWireFrame.swift
//  WeatherApp
//
//  Created by Anton Zuev on 5/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import Foundation
import UIKit

protocol MainWireFrameProtocol: class {
    func showMainModuleAsRoot()
}

class MainWireFrame {
    
    let mainViewControllerIdentifier = "MainViewController"
    let mainViewController: MainViewController!
    var rootWireFrame : RootWireFrame?
    
    init() {
        mainViewController = UIStoryboard(name: "Main", bundle: Bundle(for: type(of: self))).instantiateViewController(withIdentifier: mainViewControllerIdentifier) as? MainViewController
    }
}

extension MainWireFrame: MainWireFrameProtocol {
    
    func showMainModuleAsRoot() {
        if let mainViewController = mainViewController {
            rootWireFrame?.showRootViewController(viewController: mainViewController)
        } else {
            print("ERROR: View controller from storyboard not found")
        }
    }
}

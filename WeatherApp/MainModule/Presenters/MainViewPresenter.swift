//
//  WeatherMainViewPresenter.swift
//  WeatherApp
//
//  Created by Anton Zuev on 3/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import Foundation
import CoreLocation

protocol MainViewPresenterProtocol:class {
    func showCurrentUserPosition(for latitude: Double, longitude: Double)
    func showWeatherForecastForThisPosition(forLocation latitude: Double, longitude: Double)
    func userTouchedTheMap(touchMapLocation latitude: Double, longitude: Double)
    func getNumberOfRows() -> Int
    func getWeatherSummaryForRow(index: Int) -> String
    func getWeatherTemperatureForRow(index: Int) -> String
    func getLocationDescription() -> String?
    func autorizationStatusChanged(status: CLAuthorizationStatus)
}

extension Double {
    func getSpecialHourDateStringFromUTC() -> String {
        let date = Date(timeIntervalSince1970: self)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm, dd MMM " //Set time style
        dateFormatter.locale = Locale(identifier: "es_ES")
        let localDate = dateFormatter.string(from: date)
        return localDate
    }
}

class MainViewPresenter {
    
    var weatherInfo: [WeatherInfoHourlyData]?
    var location: CLLocation?
    var locationDescription: (String?)

    weak var view:MainViewProtocol!
    weak var mainWireFrame:MainWireFrameProtocol!
    let interactor:MainViewInteractorProtocol!
    
    init(view: MainViewProtocol,interactor: MainViewInteractorProtocol, mainWireFrame: MainWireFrameProtocol) {
        self.view=view
        self.interactor=interactor
        self.mainWireFrame=mainWireFrame
    }
}

extension MainViewPresenter: MainViewPresenterProtocol {
    /// User touched the map
    func userTouchedTheMap(touchMapLocation latitude: Double, longitude: Double) {
        showWeatherForecastForThisPosition(forLocation: latitude, longitude: longitude)
        //if there is more than one annotation, delete all
        if view.getMapViewAnnotationsCount() > 0 {
            view.removeAllMapAnnotations()
        }
        view.addAnotationView(forLocation: latitude, longitude: longitude)
    }
    ///Get number of rows in the table (aka number of weather forecast hours)
    func getNumberOfRows() -> Int {
        if let weatherInfo = weatherInfo {
            return weatherInfo.count
        }
        else {
            return 0
        }
    }
    ///Get weather summary for the specific hour
    func getWeatherSummaryForRow(index: Int) -> String {
        if let weatherInfo = weatherInfo {
            return weatherInfo[index].summary
        }
        else {
            return ""
        }
    }
    ///Get temperature for the specific hour
    func getWeatherTemperatureForRow(index: Int) -> String {
        if let weatherInfo = weatherInfo {
            return "Temperatura: \(Int(weatherInfo[index].temperature))° - Hora \(weatherInfo[index].time.getSpecialHourDateStringFromUTC())"
        }
        else {
            return ""
        }
    }
    ///Get weather forecast for specific location and update table view to show weather forecast for it
    func showWeatherForecastForThisPosition(forLocation latitude: Double, longitude: Double) {
        interactor.getFirst12OrderedWeatherForecastForLocation(latitude: latitude, longitude: longitude, completionHandler: { (weatherInfo) in
            if weatherInfo != nil {
                self.location = CLLocation(latitude: latitude, longitude: longitude)
                self.weatherInfo = weatherInfo
                self.calculateLocationDescription(completionHandler: { (locationDescription) in
                    self.view.updateTableView()
                })
            }
            else {
                self.view.showErrorMessage()
            }
        })
    }
    ///Show user location on the map
    func showCurrentUserPosition(for latitude:Double, longitude:Double) {
        view.setUserLocationOntheMap(for: latitude, longitude: longitude)
    }
    ///Get location description on the location
    func calculateLocationDescription(completionHandler:@escaping (String?)->()){
        if (self.location != nil) {
            //Get placemark object for specified location
            interactor.getPlacemarkObject(forLatitude: self.location!.coordinate.latitude, longitude: self.location!.coordinate.longitude) { (placemark) in
                if placemark != nil {
                    self.locationDescription = placemark!.locality
                    completionHandler(self.locationDescription)
                }
                else {
                    completionHandler(nil)
                }
            }
        }
    }
    
    ///Get location description for the location
    func getLocationDescription() -> String? {
        return locationDescription
    }
    func autorizationStatusChanged(status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined,
             .denied,
             .restricted:
            self.view.showLocationAlertAndSettings()
            self.view.stopActivityIndicator()
        default: break
        }
    }
}

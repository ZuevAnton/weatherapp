//
//  ViewController.swift
//  WeatherApp
//
//  Created by Anton Zuev on 3/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import UIKit
import MapKit

protocol MainViewProtocol: class {
    func setUserLocationOntheMap(for latitude: Double, longitude: Double)
    func updateTableView()
    func showErrorMessage()
    func showMessageAlert(message: String, title: String)
    func showLocationAlertAndSettings()
    func getMapViewAnnotationsCount()-> Int
    func removeAllMapAnnotations()
    func addAnotationView(forLocation latitude: Double, longitude: Double)
    func stopActivityIndicator()
}

class MainViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mapView: MKMapView!
    var activityIndicatorView: UIActivityIndicatorView!
    let locationManager = LocationUtility.shared
    var presenter: MainViewPresenterProtocol!
    let reachability = Reachability()!
    
    //User touched the map event
    @IBAction func touchHandler(_ gestureRecognizer: UITapGestureRecognizer) {
        //proceed if the state is ended
        if gestureRecognizer.state != .ended { return }
        //touch point coordinates in the map view coordinate system
        let touchPoint = gestureRecognizer.location(in: mapView)
        let touchMapCoordinate = mapView.convert(touchPoint, toCoordinateFrom: mapView)
        //tell the presenter that user touched the map
        presenter?.userTouchedTheMap(touchMapLocation: touchMapCoordinate.latitude, longitude: touchMapCoordinate.longitude)
    }
    
    override func loadView() {
        super.loadView()
        activityIndicatorView = UIActivityIndicatorView(style: .gray)
        tableView.backgroundView = activityIndicatorView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //start to receive notifications of network status
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
        reachability.whenUnreachable = { unreachability in
            self.showMessageAlert(message: NSLocalizedString("NetworkIsUnreachable", comment:""))
        }
        //add self as observer for location updates
        locationManager.addObserver(newObserver: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        activityIndicatorView.startAnimating()
        tableView.separatorStyle = .none
    }
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //remove self as observer for location updates
        locationManager.removeObserver(observerToRemove: self)
        reachability.stopNotifier()
    }
}

extension MainViewController: MainViewProtocol {
    ///Remove all map view annotations
    func removeAllMapAnnotations() {
        mapView.removeAnnotations(mapView.annotations)
    }
    ///Get count of map viewannotations
    func getMapViewAnnotationsCount() -> Int {
        return mapView.annotations.count
    }
    ///Add annotation view in the specific map location
    func addAnotationView(forLocation latitude: Double, longitude: Double) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2DMake(latitude, longitude);
        mapView.addAnnotation(annotation)
    }
    ///Update table view with forecast information
    func updateTableView() {
        DispatchQueue.main.async {
            self.activityIndicatorView.stopAnimating()
            self.tableView.separatorStyle = .singleLine
            self.tableView.reloadData()
            self.tableView.selectRow(at:IndexPath(row: 0, section: 0), animated: true, scrollPosition: UITableView.ScrollPosition.top)
        }
    }
    ///Show error message
    func showErrorMessage() {
        DispatchQueue.main.async {
            self.activityIndicatorView.stopAnimating()
            self.showMessageAlert(message: NSLocalizedString("NetworkErrorMessage", comment:""))
        }
    }
    ///Show message alert
    func showMessageAlert(message: String, title: String = NSLocalizedString("Alert", comment: "")){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            print("The \"OK\" button pressed")}))
        self.present(alert, animated: true, completion: nil)
    }
    ///Show location alert and settings
    func showLocationAlertAndSettings()
    {
        let alert = UIAlertController(title: NSLocalizedString("Alert", comment: ""), message: NSLocalizedString("AccessDenied", comment:""), preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            print("The \"OK\" button pressed")
        }))
        alert.addAction((UIAlertAction(title: NSLocalizedString("Settings", comment: ""), style: .default, handler: { _ in
            print("The \"Settings\" button pressed")
            guard let urlGeneral = URL(string: UIApplication.openSettingsURLString) else {
                return
            }
            UIApplication.shared.open(urlGeneral)
        })))
        self.present(alert, animated: true, completion: nil)
    }
    ///Show current user position on the map view
    func setUserLocationOntheMap(for latitude: Double, longitude: Double){
        self.mapView.showsUserLocation = true
        self.mapView.setCenter(CLLocationCoordinate2DMake(latitude, longitude), animated: true)
    }
    ///Stop loading activity indicator
    func stopActivityIndicator() {
        self.activityIndicatorView.stopAnimating()
    }
}

extension MainViewController: LocationObserverProtocol {
    ///Location update event. Show current user position and weather forecast for this position
    func locationUpdate(locations: [CLLocation]) {
        if let firstLocation = locations.first{
            presenter?.showCurrentUserPosition(for: firstLocation.coordinate.latitude, longitude: firstLocation.coordinate.longitude)
            presenter?.showWeatherForecastForThisPosition(forLocation: firstLocation.coordinate.latitude, longitude: firstLocation.coordinate.longitude)
        }
    }
    ///Location autorization status has changed
    func autorizationStatusDidChange(status: CLAuthorizationStatus) {
        presenter?.autorizationStatusChanged(status: status)
    }
}


extension MainViewController: UITableViewDataSource{
    ///Number of section in the table view
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1;
    }
    ///Number of weather forecast hours
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let numberOfRows = presenter?.getNumberOfRows(){
            return numberOfRows
        }
        else{
            return 0
        }
    }
    ///Show in the cell weather forecast for the specific hour (Summary and temperature)
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherInfo", for: indexPath)
        cell.textLabel?.text = presenter?.getWeatherTemperatureForRow(index: indexPath.row)
        cell.detailTextLabel?.text = presenter?.getWeatherSummaryForRow(index: indexPath.row)
        return cell;
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return presenter.getLocationDescription()
    }
}

extension MainViewController: MKMapViewDelegate{
    
    ///Show map view annotation as PinAnnotaionView
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = false
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
}


//
//  WeatherInfo.swift
//  WeatherApp
//
//  Created by Anton Zuev on 3/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//


///Structure for weather forecast information
struct WeatherInfo: Codable {
    let latitude: Double
    let longitude: Double
    let hourly: WeatherInfoHourly ///hourly information
}

///Structure for hold summary and array of hourly weather information
struct WeatherInfoHourly: Codable {
    let summary: String
    let data: [WeatherInfoHourlyData]
}

///Structure to hold summary, time (UNIX time stamp) and temperature information
struct WeatherInfoHourlyData: Codable {
    let time: Double
    let summary: String
    let temperature: Double
}





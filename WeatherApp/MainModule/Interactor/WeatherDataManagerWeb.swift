//
//  DataManager.swift
//  WeatherApp
//
//  Created by Anton Zuev on 3/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import Foundation

class WeatherDataManagerWeb: WeatherDataManagerProtocol {
    
    let key = "707f8289d92c7ddcda298935df863a11"
    let weatherServiceURL = "https://api.darksky.net/forecast/"
    
    var errorMessage = ""
    
    ///Get weather forecast for location (implementation for the web server Darsky API)
    func getWeatherForecastForLocation(latitude:Double, longitude:Double, completionHandler: @escaping (_:WeatherInfo?)->()){
        
        if var urlComponents = URLComponents(string: "\(weatherServiceURL)\(key)/\(latitude),\(longitude)") {
            urlComponents.query = "lang=es&units=si"
            guard let url = urlComponents.url else { return }
            
            NetworkTaskManager.shared.dataTask(with: url) { (data, response, error) in
            
                if let error = error {
                    self.errorMessage += "DataTask error: " + error.localizedDescription + "\n"
                    completionHandler(nil)
                } else if let data = data,
                    let response = response as? HTTPURLResponse,
                    response.statusCode == 200 {
                    if let weatherInfoObject = try? JSONDecoder().decode(WeatherInfo.self, from: data){
                        completionHandler(weatherInfoObject)
                    }
                    else{
                        completionHandler(nil)
                    }
                }
            }
        }
    }
    
}

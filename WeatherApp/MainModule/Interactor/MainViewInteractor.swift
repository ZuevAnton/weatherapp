//
//  WeatherInfoInteractor.swift
//  WeatherApp
//
//  Created by Anton Zuev on 3/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import Foundation
import CoreLocation

protocol WeatherDataManagerProtocol:class {
    func getWeatherForecastForLocation(latitude: Double, longitude: Double, completionHandler: @escaping (_:WeatherInfo?)->())
}

protocol MainViewInteractorProtocol:class {
    func getFirst12OrderedWeatherForecastForLocation(latitude: Double, longitude: Double, completionHandler: @escaping (_:[WeatherInfoHourlyData]?)->())
    func getPlacemarkObject(forLatitude latitude:Double, longitude:Double, completionHandler: @escaping (CLPlacemark?) -> Void)
}

class MainViewInteractor {
    
    let dataManager: WeatherDataManagerProtocol!
    
    init(dataManager: WeatherDataManagerProtocol) {
        self.dataManager = dataManager
    }
}

extension MainViewInteractor: MainViewInteractorProtocol{
    ///Get weather forecast for location
    /// - Parameters:
    ///     - latitude: latitude of the place
    ///     - longitude: longitude of the place
    ///     - completionHandler: block that will be executed after and will contain weather information
    func getFirst12OrderedWeatherForecastForLocation(latitude: Double, longitude: Double, completionHandler: @escaping (_:[WeatherInfoHourlyData]?)->()){
        dataManager.getWeatherForecastForLocation(latitude: latitude, longitude: longitude, completionHandler: {(weatherInfo) in
            if let weatherInfo = weatherInfo{
                //fetch first 12 records and sort them in descending order by temperature
                let sortedWeatherInfo = weatherInfo.hourly.data.prefix(12).sorted(by: { (a, b) -> Bool in
                    if a.temperature>b.temperature{
                        return true
                    }
                    else{
                        return false
                    }
                })
                completionHandler(sortedWeatherInfo)
            }
            else{
                completionHandler(nil)
            }
        })
    }
    
    ///Get placemark object for the location
    func getPlacemarkObject(forLatitude latitude:Double, longitude:Double, completionHandler: @escaping (CLPlacemark?)
        -> Void ) {
        
        let geocoder = CLGeocoder()
        // Look up the location and pass it to the completion handler
        geocoder.reverseGeocodeLocation(CLLocation(latitude: latitude, longitude: longitude),
                                        completionHandler: { (placemarks, error) in
                                            if error == nil {
                                                let firstLocation = placemarks?.first
                                                completionHandler(firstLocation)
                                            }
                                            else {
                                                print("An error occurred during geocoding.")
                                                // An error occurred during geocoding.
                                                completionHandler(nil)
                                            }
        })
    }
    
    
}

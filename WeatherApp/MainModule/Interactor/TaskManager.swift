//
//  TaskManager.swift
//  WeatherApp
//
//  Created by Anton Zuev on 5/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import Foundation

///Class that saves NSURLSession data task and their completion handlers and coordinates them
///so that for one url the data task will be executed only one time
class NetworkTaskManager {
    
    static let shared = NetworkTaskManager()
    
    let session = URLSession(configuration: .default)
    
    typealias completionHandler = (Data?, URLResponse?, Error?) -> Void
    
    var tasks = [URL: [completionHandler]]()
    
    func dataTask(with url: URL, completion: @escaping completionHandler) {
        //if there is already exists request with the same url, just save completion handler in the dictionary
        if tasks.keys.contains(url) {
            tasks[url]?.append(completion)
        } else {
            tasks[url] = [completion]
            let _ = session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
                DispatchQueue.main.async {
                    print("Finished network task \(response?.url?.absoluteString ?? "")")
                    //print("Task response \(String(data: data!, encoding: String.Encoding.utf8) ?? "Data could not be printed")")
                    
                    guard let completionHandlers = self?.tasks[url] else { return }
                    for handler in completionHandlers {
                        print("Executing completion block")
                        handler(data, response, error)
                    }
                    //after completion, remove all saved blocks and url
                    self?.tasks.removeValue(forKey: url)
                }
            }).resume()
        }
    }
    
}

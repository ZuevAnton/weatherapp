//
//  RootWireframe.swift
//  WeatherApp
//
//  Created by Anton Zuev on 5/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import Foundation
import UIKit

class RootWireFrame {
    
    var window: UIWindow?
    
    func showRootViewController(viewController: UIViewController) {
        if let window = window {
            if let navigationController = navigationControllerFromWindow(window: window) {
                navigationController.viewControllers = [viewController]
            }
            else {
                window.rootViewController = viewController
            }
        } else {
            print("ERROR: Window cannot be nil")
        }
    }
    
    private func navigationControllerFromWindow(window: UIWindow) -> UINavigationController? {
        if let navigationController = window.rootViewController as? UINavigationController {
            return navigationController
        } else {
            return nil
        }
    }
}

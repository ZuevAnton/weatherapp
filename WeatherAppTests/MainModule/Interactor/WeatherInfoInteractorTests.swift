//
//  WeatherInfoInteractorTests.swift
//  WeatherAppTests
//
//  Created by Anton Zuev on 7/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import XCTest
@testable import WeatherApp

class WeatherInfoInteractorTests: XCTestCase {

    let dataManager = FakeDataManager()
    let nilPlaceLatitude = 0.0
    let nilPlaceLongitude = 0.0
    let normarPlaceLatitude = 5.0
    let normalPlaceLongitude = -5.0
    var interactor: MainViewInteractor?
    
    
    override func setUp() {
        interactor = MainViewInteractor(dataManager: dataManager)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testIfThereisNil() {
        interactor?.getFirst12OrderedWeatherForecastForLocation(latitude: nilPlaceLatitude, longitude: nilPlaceLongitude, completionHandler: { weatherInfo in
            XCTAssertNil(weatherInfo?.count, "There is no weather for this place")
        })
    }
    
    func testOrderOfTheArray() {
        interactor?.getFirst12OrderedWeatherForecastForLocation(latitude: normarPlaceLatitude, longitude: normalPlaceLongitude, completionHandler: { (weatherInfo) in
            if let weatherInfo = weatherInfo {
                XCTAssertEqual(weatherInfo[0].temperature, 12.0, "The highest temperature is 12 degrees")
                XCTAssertEqual(weatherInfo[1].temperature, 10.0, "The second highest temperature is 10 degrees")
                XCTAssertEqual(weatherInfo.last?.temperature, 0.0, "The lowest temperature is 0 degrees")
            }
        })
    }
    
    func testNumberOfForecastInfo() {
        interactor?.getFirst12OrderedWeatherForecastForLocation(latitude: normarPlaceLatitude, longitude: normalPlaceLongitude, completionHandler: { (weatherInfo) in
            XCTAssertEqual(weatherInfo?.count, 12, "Maximum is forecast for 12 hours")
        })
    }

}

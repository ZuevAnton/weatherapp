//
//  FakeDataManager.swift
//  WeatherAppTests
//
//  Created by Anton Zuev on 7/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import Foundation
@testable import WeatherApp

class FakeDataManager: WeatherDataManagerProtocol {
    
    func getWeatherForecastForLocation(latitude: Double, longitude: Double, completionHandler: @escaping (WeatherInfo?) -> ()) {
        switch (latitude,longitude) {
        case (0,0):
            completionHandler(nil)
        default:
            completionHandler(WeatherInfo(latitude: latitude, longitude: longitude, hourly:
                WeatherInfoHourly(summary: "Sunny",
                                  data: [WeatherInfoHourlyData(time: Date().timeIntervalSince1970,summary: "Sunny", temperature:12.0),
                                         WeatherInfoHourlyData(time: Date().addingTimeInterval(3600).timeIntervalSince1970, summary: "Cloudy", temperature: 10.0),
                     WeatherInfoHourlyData(time: Date().addingTimeInterval(9800).timeIntervalSince1970, summary: "Rainy", temperature: 9.0),
                     WeatherInfoHourlyData(time: Date().addingTimeInterval(6200).timeIntervalSince1970, summary: "Rainy", temperature: 8.0),
                     WeatherInfoHourlyData(time: Date().addingTimeInterval(6200).timeIntervalSince1970, summary: "Rainy", temperature: 8.0),
                     WeatherInfoHourlyData(time: Date().addingTimeInterval(6200).timeIntervalSince1970, summary: "Rainy", temperature: 7.0),
                     WeatherInfoHourlyData(time: Date().addingTimeInterval(6200).timeIntervalSince1970, summary: "Rainy", temperature: 5.0),
                     WeatherInfoHourlyData(time: Date().addingTimeInterval(6200).timeIntervalSince1970, summary: "Rainy", temperature: 4.5),
                     WeatherInfoHourlyData(time: Date().addingTimeInterval(6200).timeIntervalSince1970, summary: "Rainy", temperature: 2.0),
                     WeatherInfoHourlyData(time: Date().addingTimeInterval(6200).timeIntervalSince1970, summary: "Rainy", temperature: 1.5),
                     WeatherInfoHourlyData(time: Date().addingTimeInterval(6200).timeIntervalSince1970, summary: "Rainy", temperature: 0.0),
                     WeatherInfoHourlyData(time: Date().addingTimeInterval(6200).timeIntervalSince1970, summary: "Rainy", temperature: 0.3),
                     WeatherInfoHourlyData(time: Date().addingTimeInterval(6200).timeIntervalSince1970, summary: "Rainy", temperature: 1.0),
                    ])))
        }
    }
    
}

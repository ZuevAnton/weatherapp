//
//  FakeInteractor.swift
//  WeatherAppTests
//
//  Created by Anton Zuev on 7/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import Foundation
import CoreLocation
@testable import WeatherApp

class FakeInteractor: MainViewInteractorProtocol {
    func getPlacemarkObject(forLatitude latitude: Double, longitude: Double, completionHandler: @escaping (CLPlacemark?) -> Void) {
        completionHandler (nil)
    }
    
    
    func getFirst12OrderedWeatherForecastForLocation(latitude: Double, longitude: Double, completionHandler: @escaping ([WeatherInfoHourlyData]?) -> ()) {
        if latitude == 0.0 && longitude == 0.0 {
            completionHandler(nil)
        }
        else {
            completionHandler ([WeatherInfoHourlyData(time: Date().timeIntervalSince1970, summary: "Rainy", temperature: 9.0),
                WeatherInfoHourlyData(time: Date().addingTimeInterval(3600).timeIntervalSince1970, summary: "Rainy", temperature: 8.0),
                WeatherInfoHourlyData(time: Date().addingTimeInterval(6200).timeIntervalSince1970, summary: "Rainy", temperature: 5.0)])
        }
    }
}

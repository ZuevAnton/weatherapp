//
//  WeatherMainViewControllerPresenterTests.swift
//  WeatherAppTests
//
//  Created by Anton Zuev on 7/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import XCTest
@testable import WeatherApp

class WeatherMainViewControllerPresenterTests: XCTestCase {

    let nilPlaceLatitude = 0.0
    let nilPlaceLongitude = 0.0
    let normalPlaceLatitude = 5.0
    let normalPlaceLongitude = -5.0
    
    let interactor = FakeInteractor()
    let view = FakeMainViewController()
    let mainWireFrame: MainWireFrame = MainWireFrame()
    
    var presenter: MainViewPresenter!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        presenter = MainViewPresenter(view: view, interactor: interactor, mainWireFrame: mainWireFrame)
    }

    func testPresenterContainsRightData() {
        self.presenter.showWeatherForecastForThisPosition(forLocation: normalPlaceLatitude, longitude:normalPlaceLongitude)
        interactor.getFirst12OrderedWeatherForecastForLocation(latitude: normalPlaceLatitude, longitude: normalPlaceLongitude) { (weatherInfo) in
            if let weatherInfo = weatherInfo {
                XCTAssertEqual(weatherInfo.count, self.presenter.getNumberOfRows(), "This quantity should be equal")
                XCTAssertEqual("Temperatura: \(Int(weatherInfo[0].temperature))° - Hora \(weatherInfo[0].time.getSpecialHourDateStringFromUTC())", self.presenter.getWeatherTemperatureForRow(index: 0),"The temperature should be equal")
                XCTAssertEqual(weatherInfo[1].summary, self.presenter.getWeatherSummaryForRow(index: 1),"The summary should be equal")
            }
        }
    }
    
    func testErrorMessage() {
        self.presenter.showWeatherForecastForThisPosition(forLocation: nilPlaceLatitude, longitude:nilPlaceLongitude)
        XCTAssertTrue(self.view.showErrorMessageCalled,"If there is no normal location, presenter should make view show error message")
    }
    
    func testPresenterUpdateView() {
        self.presenter.showWeatherForecastForThisPosition(forLocation: normalPlaceLatitude, longitude:normalPlaceLongitude)
        XCTAssertTrue(self.view.updateTableviewCalled,"If there is data, presenter should update view")
    }
    
    func testNilWeatherInfo () {
        XCTAssertEqual(0, self.presenter.getNumberOfRows(), "The weather info is nil")
        XCTAssertEqual("", self.presenter.getWeatherTemperatureForRow(index: 0),"The temperature info is ni")
        XCTAssertEqual("", self.presenter.getWeatherSummaryForRow(index: 0),"The summary info is ni")
        
    }
    
    func testSetUserLocationOtheMap() {
        self.presenter.showCurrentUserPosition(for: normalPlaceLatitude, longitude: normalPlaceLongitude)
        XCTAssertTrue(self.view.userLocationSet, "User location set function is called")
    }
    
    func testUserTouchedTheMapEvent() {
        self.presenter.userTouchedTheMap(touchMapLocation: normalPlaceLatitude, longitude: normalPlaceLongitude)
        XCTAssertEqual(self.view.annotations, 1,"User has added annotation")
        self.presenter.userTouchedTheMap(touchMapLocation: normalPlaceLatitude, longitude: normalPlaceLongitude)
        XCTAssertEqual(self.view.annotations, 1,"The maximum number of annotations is 1")
    }


}

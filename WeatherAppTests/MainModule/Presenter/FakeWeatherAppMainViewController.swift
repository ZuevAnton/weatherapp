//
//  FakeWeatherAppMainViewController.swift
//  WeatherAppTests
//
//  Created by Anton Zuev on 7/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

import Foundation
@testable import WeatherApp

class FakeMainViewController: MainViewProtocol {
    
    var annotations: Int = 0
    var updateTableviewCalled = false
    var userLocationSet = false
    var showErrorMessageCalled = false
    var showMessageAlertCalled = false
    var showLocationAlertAndSetingsCalled = false
    var stopActivityIndicatorCalled = false
    
    func showMessageAlert(message: String, title: String) {
        showMessageAlertCalled = true
    }
    
    func showLocationAlertAndSettings() {
        showLocationAlertAndSetingsCalled = true
    }
    
    func stopActivityIndicator() {
        stopActivityIndicatorCalled = true
    }
    
    func showErrorMessage() {
        showErrorMessageCalled = true
    }
    
    func setUserLocationOntheMap(for latitude: Double, longitude: Double) {
        userLocationSet = true
    }
    
    func updateTableView() {
        updateTableviewCalled = true
    }
    
    func getMapViewAnnotationsCount() -> Int {
        return annotations
    }
    
    func removeAllMapAnnotations() {
        annotations = 0
    }
    
    func addAnotationView(forLocation latitude: Double, longitude: Double) {
        annotations = annotations + 1
    }
    
}

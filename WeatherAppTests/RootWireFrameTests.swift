//
//  RootWireFrameTests.swift
//  WeatherAppTests
//
//  Created by Anton Zuev on 7/12/18.
//  Copyright © 2018 Anton Zuev. All rights reserved.
//

@testable import WeatherApp
import XCTest

class RootWireFrameTests: XCTestCase {

    var rootWireFrame: RootWireFrame?
    
    override func setUp() {
        super.setUp()
        rootWireFrame = RootWireFrame()
        rootWireFrame?.window = UIWindow()
        rootWireFrame?.window?.rootViewController = UINavigationController();
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testShowRootViewController() {
        let controller = UIViewController()
        
        rootWireFrame?.showRootViewController(viewController: controller)
        
        if let window = rootWireFrame?.window {
            if let rootVC = window.rootViewController as? UINavigationController {
                if let fisrtVC = rootVC.viewControllers.first{
                    XCTAssertEqual(fisrtVC, controller, "rootViewController should be the one I set")
                } else {
                    XCTFail("the first element of the viewcontrollers array must be a UIViewController")
                }
            } else {
                XCTFail("window's root view controller must be a navigation controller")
            }
        } else {
            XCTFail("root wireframe must have a window")
        }
    }
}

##Proyecto de prueba pequeño ** WeatherApp **

En esta aplicación podemos ver el pronóstico del tiempo en nuestra ubicación o en cualquier lugar del mundo. Cuando se abre por primera vez, la aplicación muestra el pronóstico del tiempo para la ubicación del usuario. Pero también podemos elegir cada lugar del mundo tocando el mapa. La información del pronóstico del tiempo es proporcionada por el servicio Darksky. La API de Dark Sky le permite buscar el clima en cualquier parte del mundo y regresar (donde esté disponible):

- Condiciones climatológicas actuales
- Pronósticos minuto a minuto a una hora
- Pronósticos hora por hora y día por día a siete días
- Observaciones hora por hora y día por día que se remontan a décadas atrás
- Alertas de clima severo en los Estados Unidos, Canadá, naciones miembros de la Unión Europea, Israel y etc.
 
##Arquitectura
 
En esta aplicación utilizo la arquitectura VIPER. Es una aplicación muy simple y contiene un solo módulo (Módulo principal). La inicialización de todas las capas del módulo único ocurre en AppInitializer. Es muy visual y conveniente. RootWireFrame es enrutador de toda la aplicación. Podemos usarlo para obtener el objeto de la ventana o el controlador de navegación raíz (si existe) o para establecer el objeto del controlador de vista como el controlador de vista de la ventana. MainWireFrame es enrutador del módulo principal. En nuestra aplicación, utiliza RootWireFrame para configurar el módulo principal como un módulo raíz.
 
##Librerías
 
En este proyecto, uso la librería Reachability para el estado de recarga de prueba de la red.
 
##Pruebas de unidad
 
He escrito pruebas unitarias para Interactor y Presentador. Para probar ambos, creé una vista falsa y un administrador de datos falso.


